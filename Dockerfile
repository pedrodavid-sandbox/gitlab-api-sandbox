FROM node:15.3.0

ENV CLI_INSTALL_PATH=/opt/tools
WORKDIR $CLI_INSTALL_PATH

COPY [ \
	"package.json", \
        "package-lock.json", \
        "./" \
]

RUN npm install

ENV PATH="${PATH}:${CLI_INSTALL_PATH}/node_modules/.bin"
